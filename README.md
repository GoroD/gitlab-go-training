# Golang at GitLab

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Hello World](#hello-world)
    - [Ruby](#ruby)
    - [Golang](#golang)
- [Golang at GitLab](#golang-at-gitlab)
- [Basics](#basics)
    - [Comments](#comments)
    - [Whitespace](#whitespace)
    - [Variables](#variables)
    - [Types](#types)
        - [Pointer syntax](#pointer-syntax)
        - [Arrays and Slices](#arrays-and-slices)
        - [Maps](#maps)
        - [Constants](#constants)
        - [Enums / Symbols / Atoms](#enums-symbols-atoms)
    - [Operators](#operators)
    - [Scopes](#scopes)
    - [Conditionals](#conditionals)
    - [Looping, iteration, enumeration](#looping-iteration-enumeration)
    - [Case / switch](#case-switch)
- [Code organisation](#code-organisation)
    - [Packages and importing](#packages-and-importing)
    - [Functions and methods](#functions-and-methods)
    - [Structs](#structs)
    - [Interfaces](#interfaces)
    - [Defer](#defer)
    - [Error handling](#error-handling)
- [Concurrency](#concurrency)
    - [Goroutines](#goroutines)
    - [Synchronization](#synchronization)
    - [Channels](#channels)
- [Tooling and environment](#tooling-and-environment)
    - [`$GOROOT` and `$GOPATH`](#goroot-and-gopath)
    - [GVM (Go Version Manager)](#gvm-go-version-manager)
    - [Dependency management](#dependency-management)
        - [Godep](#godep)
        - [Govendor](#govendor)
    - [Documentation](#documentation)
    - [Linting and checking](#linting-and-checking)
- [Resources](#resources)
    - [Awesome reading](#awesome-reading)
    - [Awesome libraries](#awesome-libraries)
    - [Editor war \o/](#editor-war-%5Co)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

An introduction for people who sort of know some Ruby.


## Hello World

Major differences:

* Indentation uses `\t`, rather than two spaces
* Code is organised into packages. This is not optional
* (Almost) nothing is imported by default
* Imports are (almost) always scoped by package name
* Capitalised identifiers are exports, rather than constants
* Compiled, rather than interpreted
* Statically, rather than dynamically, typed
* Pass-by-value, rather than pass-by-reference
* You can use pointers and references

### Ruby

```ruby
puts "Hello world!"
```

```
$ ruby hello.rb
Hello world!
```

### Golang
[(playground)](https://play.golang.org/p/xtFNtF7ZmR)

```go
package main

import (
	"fmt"
)

func main() {
	fmt.Println("Hello world!")
}
```

```
$ go run hello.go
Hello world!
```

## Golang at GitLab

* [gitlab-ci-multi-runner](https://gitlab.com/gitlab-org/gitlab-ci-multi-runner)
* [gitlab-pages](https://gitlab.com/gitlab-org/gitlab-pages)
* [gitlab-workhorse](https://gitlab.com/gitlab-org/gitlab-workhorse)
* [gitaly](https://gitlab.com/gitlab-org/gitaly)


## Basics

### Comments

In Golang, comments use C-style syntax:

```ruby
# This is a single-line comment

=begin
This is a multi-line comment
but it's rarely used
=end

# This is
# Much more common
```

```go
// This is a single-line comment

/*
 * This is a multi-line comment
 * but it's rarely used
 */

// This is
// much more common
```

### Whitespace

Like Ruby, whitespce is mostly irrelevant. Unlike Ruby, there are very strong
standards about how code should be formatted, embodied in a tool called `go fmt`

The only whitespace that's significant to the compiler is the newline - `\n`.
Go is actually a semicolon-delimited language, but instead of forcing you to
put them in yourself, Go automatically puts one before newlines. Don't worry
about it, it only has one major implication:

```ruby
# Valid
a = {
  foo: "bar"
}

# Valid
b = { foo: "bar" }

# Valid
c = {
  foo: "bar",
}
```

```go
// syntax error: unexpected semicolon or newline, expecting comma or }
a := map[string]string{
  "foo":"bar"
}

// Valid
b := map[string]string{"foo": "bar"}

// Valid
c := map[string]string{
  "foo":"bar",
}
```



### Variables

```ruby
x = 0
```

```go
var x int // These are all equivalent
x := 0
x := new(int)  // Except this returns a pointer to an int
x := *new(int)
```

In both these examples, a variable `x` is declared. In Ruby, it has a (dynamic)
type of `Fixnum`. In Go, it has a (static) type of `int`. In Go, any future
assignment to this variable must be of a matching type. For `int`, that's more
`int`s.

Extremely frustrating:

```go
var x int64
var y int

x = y
y = x

// Error messages:
// cannot use y (type int) as type int64 in assignment
// cannot use x (type int64) as type int in assignment

x = int64(y)
y = int(x)
```

On the other hand, you can be sure that `x` will never be a `string`, or
something even crazier.

Also like Ruby, the 'blank' assignment variable - `_` - exists:

```go
_ = x
```

This is much more important in Go than Ruby, because the compiler demands
that *all* variables are used. When debugging code, if the compiler complains,
just add a line like the above.

Like Ruby, Go is garbage-collected, so when you declare a variable, you
don't need to keep track of it. Declaration allocates memory, but the
garbage collector will keep track of it.

Like Ruby, allocated memory is always set to the `empty value` for the
type. 0, the empty string, etc. So no need to worry about uninitialized
memory errors.

Go also has bounds checking, so no need to worry about buffer overflows.
Mostly. An out-of-bounds access will terminate the program, but without
being exploitable.

### Types

Ruby and Go share a range of types, but some are unique to each. Most of
the differences are down to Ruby being dynamically typed with runtime
evaluation, and Go being statically typed with (mostly) compile-time
evaluation.

Go has many built-in types:

* `bool` - `true` or `false`
* `byte` - 8 bits, unsigned
* `[u]int[8|16|32|64]` - n bits, signed or unsigned
* `rune` - a Unicode character
* `string` - a UTF-8 string
* `error` - an interface
* `*T` - a pointer-to-T type

It also allows you to declare your own types:

* `T` - a custom type based on a built-in type
* `[n]T` - an `n`-sized array of `T`
* `[]T` - a *slice* of `T`
* `map[T1]T2` - a *map* of `T1 => T2`
* `struct T` - a *struct* named `T`
* `T interface` - an *interface* named T
* `func(Tin...) Tout` - a *function* with the given signature
* `chan T` - a *channel* for values of type `T`

Here are some examples of type declaration:

```go
type T1 byte
type T2 [16]byte
type T3 []byte
type T4 map[string]bool
type T5 struct {
    A string
    B string
}

type T6 interface {}

type T7 func(x, y byte)

type T8 chan byte
```

Some of these types represent a large block of memory; others are reference
or pointer types. Map types are extra-special - you need to allocate memory
for them at runtime like this: `myMap := make(map[T1]T2)`.

Since Go has pointers, it also has `nil`. Further, it has `typed nils`. Which
is to say that this doesn't work:

```go
var x *string // Empty value of a pointer is nil
var y *int

x == y // Compiler error: mismatched types *string and *int
```

You can also explicitly initialize variables with something other than the
empty value. We saw an example for `int` above - here's some of the others:

```go
x1 := T1(1)
x2 := T2{1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16}

x3a := T3{1,2,3}
x3b := make(T3)

x4a := T4{
    "foo": true,
    "bar": false,
}
x4b := make(T4)

x5a := T5{A: "Foo", B: "Bar"}
x5b := T5{"Foo", "Bar"} // If you specify all attributes, in order

var x6 T6 = otherThing
var x7 T7 = otherFunc

x8 := make(T8)
```

You can also declare (and optionally initialize) an *anonymous* type anywhere
you would use a named type, e.g:

```go
x1 := byte(1)
x2 := [16]byte{1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16}

x3a := []byte{1,2,3}
x3b := make([]byte)

x4a := map[string]bool{
    "foo": true,
    "bar": false,
}
x4b := make(map[string]bool)

x5 := struct{
    A string
    B string
} {
    A: "Foo",
    B: "Bar",
}

var x6 interface{} = otherThing
var x7 func(x, y byte) = otherFunc
```

Most declarations and initializations tend to take this form. Named types
are generally reserved for structs, interfaces, and particularly complicated
maps and slices.

Structs can, of course, contain structs, and be initialized too. Even
with anonymous structs thrown in:

```go
type T5 struct {
    Nested struct {
        A string
        B string
    }
    C string
}

x4 := T5{
   	Nested: struct{
		A string
		B string
	} {
		A: "Foo",
		B: "Bar",
	},
	C: "Baz",
}
```

Go treats anonymous types as the same type if they match in all particulars.

#### Pointer syntax

Go inherits its pointer syntax from C. `&` is used to take an address, while
`*` is used to dereference a pointer (access the value being pointed to).
Some examples:

```go
var x int

ptr1 := &x // Take the address of x, an int, and put it into `ptr1`, a *int
ptr2 := &ptr1 // Take the address of ptr1, a *int, and put it into ptr2, a **int
ptr3 := *ptr2 // Dereference ptr2, a **int, and put it into ptr3, a *int
y := *ptr3 // Dereference ptr3, a *int, and put it into y, an int
```

Fortunately, Go doesn't have pointer arithmetic. Nor does it have a `->`
operator - you can use `.` to access attributes and methods of something,
whether you have a pointer-to-x or x itself.

#### Arrays and Slices

Go has both arrays and slices. Slices are variably-sized arrays... sort of.
You want to use slices basically all the time. Even when the data is fixed-size,
if it's not tiny, use a slice.

`[16]byte` is a region of memory 16 bytes in size. `[1024*1024]byte` is a region
of memory 1MiB in size. Go is pass-by-value, which gets expensive quickly.

A slice is a very small data structure containing:

* Pointer to a region of memory
* Capacity (size of region of memory)
* Length (amount of memory used so far)

When the region of memory backing a slice is full, if you try to add more
elements to it, then the memory is *copied* to the start of a larger
region, and the capacity and length are updated with new values. Capacity
doubles every time it's exceeded.

```go
var nilSlice []byte

len(nilSlice) // 0
cap(nilSlice) // 0
nilSlice[0]   // panic

slice := make([]byte, 0)

len(slice) // 0
cap(slice) // 0
slice[0]   // panic

longSlice := make([]byte, 16)

len(longSlice) // 16
cap(longslice) // 16
longSlice[0]   // 0
longSlice[16]  // panic

capSlice := make([]byte, 0, 16)

len(capSlice)  // 0
cap(capSlice)  // 16
capSlice[0]    // panic
```

The capacity is just a hint. The length is the important attribute - never try
to read outside the value of len(slice), and never try to access a zero-length
slice.

Making a slice longer uses the `append` built-in function:

```ruby
a = [1,2,3]
a << 4
a.push(5, 6)
```

```go
a := []int{1,2,3}
a = append(a, 4)
a = append(a, 5, 6)
```

Note that while Ruby modifies its array in-place, Go returns a **new slice**.

```go
a := make([]int, 0, 1)
b = append(a, 1)
len(a) // 0
len(b) // 1
```

However, both slices point to the same underlying region of memory! It's a copy,
including a copy of that pointer's value.

```go
a := make([]int, 1, 2)
b := append(a, 2)

a[1] // panic
b[0] // 0
a[0] = 1
b[0] // 1
```

If you try to `append` past the capacity of the slice, the copy happens:

```go
a := make([]int, 1, 1)
b := append(a, 2)

a[1] // panic
b[0] // 0
a[0] = 1
b[0] // 0
```

Now, `a` and `b` don't point to the same region of memory any more. This
can seem very funky, so it's best to throw away old slices immediately!

One exception to this is creating new slices *without* doing any more
allocations. This is similar to Ruby's array indexing, although we now
know how it works under the hood!

```ruby
a = [1,2,3,4,5]
b = a[0..1]
c = a[1..-1]
d = a[-3..-2]
```

```go
a := []int{1,2,3,4,5}
b := a[0:1]
c := a[1:]
d := a[len(a)-3:len(a)-1]
```

In Ruby, all the values are being copied. In Go, just the slice is being
copied. The underlying memory region is shared between them all.

You can use the `copy` built-in if this is really inconvenient:

```go
a := []int{1,2,3,4,5}
b := make([]int, len(a)-1)
copy(dst, src[1:])
```

This prevents tricks like this:

```go
a := []int{1,2,3}
b := a[0:1]
c := b[0:cap(b)]
c[2] // 3
```

#### Maps

Go's maps are Ruby's hashes. Use them to store key-value data, lookup tables,
etc. *Don't* use them to pass arguments to functions, at least, not in general.

```ruby
hash = {a: 1, b: 2}
```

```go
hash := map[string]int{
    "a": 1,
    "b": 2,
}
```

Sets are also commonly implemented with a map in Go, e.g:

```ruby
require 'set'

set = Set.new
```

```go
set := make(map[string]struct{})
```

Attempting to read from, or assign to, a nil map will result in a panic.

If you know how many items you're likely to put in the map, you can
optimise the allocation:

```go
make(map[string]struct{}, 16) // set length to 0, capacity to 16
```

Interacting with a map is quite similar to a Ruby hash:

```ruby
h = Hash.new
h["key"] = "value"
value = h["key"]
h.has_key?("key")
h.size
```

```go
h := make(map[string]string)
h["key"] = "value"
value := h["key"]
_, ok := h["key"]
len(h)
```


#### Constants

In Ruby, any identifier beginning with a capital letter (in any language) are
constants. Some examples:

```ruby
class Foo       # Foo is a constant. Classes don't have to be put into constants
  TEXT = "foo"  # TEXT is also a constant, being all-caps
  enemy = "bar" # Local variable. Probably.
end
```

```go
const (
    Text = "foo"  // This is an exported constant
    enemy = "bar" // This is an unexported constant
)

type Foo struct {} // Not a constant

a = 30 // The value side of this is also a constant
```

The *type* of a constant in Go is much more flexible than you might expect:

```go
const numeric = 10
const text = "foo"

type MyNumeric uint64
type MyString string

var (
  a int8      = numeric
  b int16     = numeric
  c int32     = numeric
  d int64     = numeric
  e MyNumeric = numeric
  f string    = text
  g Mystring  = text
)
```

By default, constants don't have an explicit type. At compile time, if the value
reasonably fits with the type of the variable being assigned to, a *typed copy*
of it will be made and used in the assignment.

The constants themselves are eliminated at compile time, so they don't exist
when your program is running. So you can't get a pointer to them.

You can also specify a specific type for a constant if you want to do avoid this
flexibility. This is used in the standard library's `time` package:

```go
const (
	Nanosecond  Duration = 1
	Microsecond          = 1000 * Nanosecond
	Millisecond          = 1000 * Microsecond
	Second               = 1000 * Millisecond
	Minute               = 60 * Second
	Hour                 = 60 * Minute
)
```

You can perform normal arithmetic with `Duration`s and untyped constants, but
you can't mix the types:

```go
var a int64 = 60

a * time.Second // invalid operation: a * time.Second (mismatched types int64 and time.Duration)
```

#### Enums / Symbols / Atoms

Constants can be used to create something that acts a bit like symbols in Ruby:

```ruby
CONNECTIVITY_STATES = [
  :idle,
  :connecting,
  :ready,
  :transient_failure,
  :shutdown
]
```

```go
// Example from google.golang.org/grpc
type ConnectivityState int

const (
	Idle ConnectivityState = iota
	Connecting
	Ready
	TransientFailure
	Shutdown
)
```

`iota` is a magic word. The actual value of the `Idle` constant is 0. All the
constants below it get an automatically incrementing value.

Introducing a type means that `int(0)` and `ConnectivityState(0)` can never be
confused.

### Operators

These mostly have the same effect between Ruby and Go, but here are some
differences:

* Ruby has operator overloading (`def + ... end`); Go does not
* Ruby treats operators as methods; Go does not
* Ruby has the ternary operator (`x ? y : z`); Go does not
* Ruby has the exponent operator (`**`); Go does not
* Ruby has the spaceship (`<=>`) operator; Go does not
* Go has the postfix operator (`++`); Ruby does not
* Go separates initial (`:=`) and subsequent (`=`) assignment; Ruby does not


### Scopes

```ruby
begin
  # commands
rescue
  # catch exceptions
ensure
  # unconditionally execute when leaving scope
end
```

```go
{
    // unconditionally execute when leaving scope
    defer func() {
        // recover from a panic
        if err := recover(); err != nil {
        }
    }()
}
```

### Conditionals

```ruby
# x may be anything. Only nil and false evaluate to false
if x
elsif y
else
end

# unless keyword
unless x
end

# Trailing conditionals
do_it if x
do_it unless x

# ternary operator
x ? do_it : do_not_do_it
```

```go
// the type of x *must* be bool
if x {
} else if y {
} else {
}

// no unless keyword
if !x {
}

// No trailing conditionals
// No ternary operator
```

### Looping, iteration, enumeration

```ruby
# Loop forever
loop do
  break if ... # Stop looping
  continue if ... # start a new iteration
end

# While loops
while x
  do_it
end

# Until loops
until x
  do_it
end

# Trailing while/until
do_it while x
do_it until x

# C-style for loops are out of fashion
0.upto(99) {|i| ... }

# And while for exists, noody uses it
for i in collection do
end


# Much more common is to use iterators
collection.each {|item| ... }
collection.each_with_index {|item, index| ... }
collection.map {|item| transform(item) }
hash.each {|k, v| ... }
```

```go
// Loop forever
for {
    if ... {
        break // Stop looping
    }

    if ... {
        continue // start a new iteration
    }
}

// No while
// No until

// C-style for loops are very much in fashion, and replace while/until
for i := 0; i < 100; i++  { ... }
for i := 0;        ; i++  { ... }
for i := 0; i < 100;    ; { ... }

// Everyone uses `range`, which is like `in`, for everything
for index, item := range slice { ... }

// Special case: using range over a string produces runes
for _, rune := range str { ... }

// We don't see many iterators. Here's the equivalent of `#map`
var x []T
for index, item := range collection {
    x = append(x, transform(item))
}

// Operating on a map
for k, v := range hash { ... }
```

### Case / switch

```ruby
# Case statements checking the value of x
case x
when 1
when 2
else
end

# Case statements checking the type of y
case y
when String
when Fixnum
end

```
```go
// switch statement checking the value of x
switch x {
case 1:
case 2:
default:
}

// switch statement checking the concrete type of y, which is an interface
switch typedY := y.(type) {
case string:
case T:
default:
}
```

## Code organisation

At first glance, Go has a much more procedural feel to it than Ruby. There are
many functions that would be methods, they tend to be much longer, and explicit
error handling is everywhere! There are also no "real" inheritance mechanisms,
and metaprogramming is basically non-existent.

So what gives? How do you design Go code that is easy to understand, reusable,
and a joy to refactor? Let's start with the means we have to organise our code:

### Packages and importing

A `package` is a collection of `.go` files in a single directory. They form a
namespace and may be imported into other packages. Package directories are
expected to be placed within a `vendor/` directory, the `$GOPATH` or `$GOROOT`,
and have an *import path* based on that directory structure, e.g:

```
/usr/share/go/src/pkg/
    fmt
/home/lupine/go/src/
    github.com/
        BurntSushi/toml
    gitlab.com/
        gitlab-org/gitlab-workhorse/
             internal/helpers
        vendor/
            github.com/gorilla/websocket
```

Importing all these packages would look like:

```go
import (
    "fmt"

    "github.com/BurntSushi/toml"
    "github.com/gorilla/websocket"

    "gitlab.com/gitlab-org/gitlab-workhorse/internal/helpers"
)
```

The special `main` package in `gitlab.com/gitlab-org/gitlab-workhorse`
indicates to the compiler that it should produce an executable with an
entry point starting at `func main() { ... }`. Program flow always begins
in one of those.

TODO: file naming conventions, how to split things into files. File scope.

A directory may only contain a single package, plus a `_test` package, e.g.:

* `foo.go` -> `package thingy`
* `bar.go` -> `package thingy`
* `thingy_test.go` -> `package thingy`
* `bar_test.go` -> `package thingy_test`

Test files aren't included into your binaries, only when building tests.

You *can* declare packages with a different name to their directory, or
override the package name when importing them, but you should normally avoid
both.

Packages may contain exported or un-exported things - structs, constants,
variables. Anything `Capitalised` is exported. Everything else is not.


```go
package foo

type Foo struct{}

var (
    DefaultFoo  = Foo{}
    nonDefaultFoo = Foo{}
)

const (
    NotFoo = "bar"
    notBar = "foo"
)

func NewFoo() *Foo {
    // access check, or anything else

    return newFoo()
}

func newFoo() *Foo {
    return &Foo{}
}
```

When using something from a package you've imported, you always need to
use the namespace. `fmt.Println` is the exported function `Println` in
the namespace `fmt`, which *will* be declared in an import block.

Packages can be imported just for their side effects, like:

```go
import (
    "database/sql"

    _ "github.com/lib/pq"
)
```

What sort of side effects? Mostly, execution of `init()` functions. When
imported, any `init()` functions in the package will be executed (in
haphazard order). `database/sql` uses this to maintain a list of installed
driver plugins.

You can have multiple `init()` functions in a package - or even a file -
and they will all run. Generally avoid this.

```go
// foo.go
package main

import "fmt"

func init() {
    fmt.Println("Foo init 1")
}


func init() {
    fmt.Println("Foo init 2")
}
```

```go
// bar.go
package main

import "fmt"

func init() {
    fmt.Println("Bar init")
}

func main() {
    fmt.Println("Hello")
}
```

```sh
$ go run bar.go foo.go
Bar init
Foo init 1
Foo init 2
Hello
$ go run foo.go bar.go
Foo init 1
Foo init 2
Bar init
Hello
$
```

### Functions and methods

This is an area with stark philosophical differences between Ruby and
Go, but it doesn't matter **too** much. Both allow you to organise
your code into reusable units, and both allow you to pass those units
around:

```ruby
require 'base64'

mult = ->(a, b) { a * b }
mult.call(2, 2) # 4

b64 = Base64.method(:encode)
b64.call("foo") # "Zm9v\n"
```

```go
package main

import "encoding/base64"

func main() {
    mult := func(a, b int) int { return a * b }
    _ = mult(2, 2) // 4

    b64 := base64.StdEncoding.EncodeToString
    _ = b64("foo") // "Zm9v"
}
```

It's fairly common in Go to treat the package as something "a bit like"
a class. Class methods in Ruby are functions exported from the package,
while instance methods in Ruby are methods of a type in Go, e.g.:

```ruby
class Foo
  def self.to_s
    "foo"
  end

  def opponent
    read_opponent
  end

  private

  def read_opponent
    "bar"
  end
end

# Foo.to_s
# Foo.new.opponent
```

```go
package foo

type Foo struct{}

func String() string {
    return "foo"
}

func NewFoo() Foo {
    return Foo{}
}

func (f Foo) Opponent() string {
    return f.readOpponent()
}

func (f Foo) readOpponent() string {
    return "bar"
}

// foo.String()
// foo.NewFoo().Opponent()
```
Unlike Ruby, there's really no safe way to get hold of private methods in Go.
If the package doesn't export it in some way, you're out of luck. This means
that you should export things unless there's a *really* good reason not to.

### Structs

Go doesn't have classes, it has structs. They're quite different - much more
focused on data than behaviourThey are types that contain attributes and,
like other Go types, may also have methods attached. You can also **embed**
one struct in another. Superficially, this looks like inheritance:

```ruby
class Empty
  def ancestor_method
    self.class
  end
end

class X < Empty
  attr_accessor :exported

  private

  attr_accessor :unexported
end
```

```go
type Empty struct{}

func (e Empty) AncestorMethod() Empty {
    return e
}

type X struct {
    Empty // Embeds an attribute called Empty, of type Empty
    Exported string
    unexported string
}
```

Look again, however. In Ruby, `X.new.ancestor_method` will return `X`, and
`Empty.new.ancestor_method` will return `Empty`. In Go, it will always
return a struct of type `Empty`. So avoid abstract base classes, in general.

As in Ruby, if the `AncestorMethod()` method isn't defined on `X`, Go will
look at the embedded struct `Empty` to fulfil the call, but it's just
calling `instance.Empty.AncestorMethod()`. It's more akin to this:

```ruby
class Empty
  def ancestor_method
    self.class
  end
end

class X
  attr_accessor :empty, :exported

  delegate :ancestor_method, to: :empty
end

X.new(empty: Empty.new) # We'd need to write #initialize too...
```

This is composition, rather than inheritance. So you need to think in those
terms when understanding or writing Go code. If you try to make inheritance
work, it will be messy.

The empty struct - `struct{}` - is a special value that takes up 0 bytes.
It's often used instead of booleans or subclasses of `Object` when there
is no state to be tracked.

### Interfaces

An interface is a pointer type that lists a series of methods. A value
**satisfies** that interface if it has all those methods.

This is used in places where Ruby would use "duck typing" - as Go is
statically typed, some idea of the type of variables is required at
compile-time, and interface types satisfy that requirement while still
allowing flexibility of concrete types.

```ruby
class X
  def read
  end

  def close
  end
end
```

```go
type Reader interface {
    Read([]byte) (int, error)
}

type Closer interface {
    Close() error
}

type ReadCloser interface {
    Reader
    Closer
}

type X struct{}

func (x *X) Read([]byte) (int, error) {
    return 0, nil
}

func (x *X) Close() error {
    return nil
}
```

This is a Go [standard library](https://golang.org/pkg/io/#Reader) example
and you'll find these interfaces everywhere. The standard library has a
wide range of functions that take an `io.Reader` or `io.Closer`; by making
your custom types satisfy the interface, you can pass them into those stdlib
functions. There's also an `io.Writer` interface, of course.

Like structs, interfaces can be formed by embedding other interfaces. In general,
interfaces should be as few methods as possible, though. One-method interfaces
are the holy grail, and are named according to the "doer" convention where
possible.

A common mantra in Go is that functions should take interface types and return
concrete types. This isn't a hard-and-fast rule, but it is an expression of
the Dependency Inversion Principle that's worth considering.

If you're doing TDD, you'll tend to discover interfaces naturally as you try
to stub out external network dependencies, third-party libraries, etc, in
your tests.

This is another abstraction pushing code towards composition, rather than
inheritance. As long as you preserve the interface, you can wrap any type
to add functionality to it, e.g.:

```go
package main

import (
    "io"
    "net/http"
    "os"
)

type CountingReader struct {
    io.Reader
    Count int
}

func (c *CountingReader) Read(data []byte) (int, error) {
    n, err := c.Reader.Read(data)
    c.Count += n
    return n, err
}

func main() {
    var readers []io.Reader
    var closers []io.Closer

    defer func() {
        for _, closer := range closers {
            closer.Close()
        }
    }()

    for _, filename := range os.Args[1:] {
        file, _ := os.Open(filename)
        readers = append(readers, file)
        closers = append(closers, file)
    }

    onDisc, _ := os.Create("on-disc")
    closers = append(closers, onDisc)

    multi := io.MultiReader(readers...)
    counting := &CountingReader{Reader: multi}
    teeing := io.TeeReader(counting, onDisc)

    http.Post("https://example.com", "application/octet-stream", teeing)
}
```

Because all these diverse types satisfy `io.Reader`, we can trivially create a
program that:

* Takes a list of filenames on the command line
* Assembles them into a virtual concatenated data stream
* Counts the total number of bytes in that stream
* Simultaneously writes the data to a file and a web service
* Calls `Close()` on all our file descriptors.

We could add more behaviour without changing the code of any of the existing
components, just by creating a new struct that satisfies the interface and
plugging it into the right part of our pipeline. This is good.

We could even replace our `onDisc` variable with anything implementing
`io.WriteCloser` interface (as we use both `Write()` and `Close()` on it).
Another network service, for instance. We could even wrap it with an
`io.MultiWriter` and allow us to send the written data to a range of places
simultaneously.

One more interface: `error` is an interface type. It looks like this (with
some compiler magic):

```go
type error interface {
    Error() string
}
```

So, you can create your own custom error types that can be passed through any
code expecting `error`, but which *also* carry additional information. As you
might expect, this is used extensively in the standard library:

```ruby
case err
when PathError
  puts "Bad path: #{err.path}"
when OpError
  puts "Timed out" if err.timeout?
else
  puts err.inspect
end
```

```go
switch err.(type) {
case pathErr := *os.PathError:
    fmt.Println("Bad path:", pathErr.Path)
case opErr := *net.OpError:
    if opErr.Timeout() {
        fmt.Println("timed out")
    }
default:
    fmt.Println(err.Error())
}
```

Here, we use a case statement that checks the concrete type (Go) or
class (Ruby) of `err`. In Ruby, a `NoMethodError` at runtime is
possible,  but in Go, if `os.PathError` loses the `Path` attribute at
some point, the compiler will spot it for you. Hooray!

The compiler can be partially bypassed by using type assertions, e.g.:

```go
// may panic
pathErr := err.(*os.PathError)

// safe
if pathErr, ok := err.(os.PathError) { ... }
```

These cases will still allow the compiler to spot the "`Path` attribute
was removed" case, but the first will panic if the concrete type of `err`
is *not* `*os.PathError`. Runtime type checking needs runtime error
handling, so prefer the second basically all the time!

### Defer

The Go version of `ensure` guards:

```ruby
def open
  file = File.new
  yield(file)
ensure
  file.close
end
```

```go
func Open(f func(File)) {
    file = NewFile()
    defer file.Close()

    f(file)
}
```

This is an idiomatic way to clean up resources in Go. Placing the cleanup
right next to the allocation makes it harder to forget about. Defer calls
are executed even if a panic occurs.

### Error handling

Go doesn't have exceptions in the Ruby sense. Instead, explicit `error` values
are returned by functions and methods; callers are expected to check for
errors and react appropriately:

```ruby
def rename_thing
  thing = Thing.find_by(id: 1)
  thing.update_attributes!(name: "Thiiing") if thing.present?
end
```

```go
func RenameThing() error {
	thing, err := db.FindThingById(1)
	if err != nil {
		return err
	}

	thing.Name = "Thiiing"

	return db.UpdateThing(thing)
}
```

In the Ruby example, `find_by` may return a thing, or it may raise an exception.
In Go, most exceptional cases - including "no thing by that ID" - are given in
the second return value instead. Errors are expected to be "bubbled up" the
call graph, and handled in the most appropriate location.

The `error` type is an interface, so you can create your own custom errors
to represent common responses and return additional metadata.

Go does have a *rudimentary* form of exception, which it calls `panic`:

```go
func bad() {
	panic("Something is wrong!")
}

func do() {
	defer func() {
		if err := recover(); err != nil {
			fmt.Println("Recovered from error:", err)
		}
	}()

	bad()
}
```

On the surface, this is similar to Ruby's raise/rescue idiom, but avoid using it
as such! Panics are not generally intended to be handled by callers, and if they
reach the top level of a goroutine, the whole process is terminated with useful
debugging output, including a stack trace.

An instructive exception is the `net/http` stdlib server - each HTTP request is
handled in its own goroutine. If a request panics, it is recovered before it
kills the whole server, allowing that request to be aborted without affecting
any others.

An unhandled panic will give you a backtrace. For a program like this:

```go
package main

func a() {
    b()
}

func b() {
    c()
}

func c() {
    panic("error")
}

func main() {
    a()
}
```

You might get a backtrace like this:

```
panic: error

goroutine 1 [running]:
panic(0x56d80, 0xc82000a100)
	panic.go:481 +0x3e6
main.c()
	main.go:12 +0x65
main.b()
	main.go:8 +0x14
main.a()
	main.go:4 +0x14
main.main()
	main.go:16 +0x14
exit status 2
```

The information you get includes the ID of the goroutine that paniced, and its
state; the a set of stack frames. Like Ruby, the most recent stack frame is
first, and as you go down the list, they get older.

Each stack frame tells you the function that panicked, the value of any
arguments, then the filename and line number where that frame was recorded.
The memory addresses and offsets can be used if you start digging into `gdb`,
`delve` or `godebug`, but that's rare.

## Concurrency

In Ruby, concurrency is often approached by having many processes (e.g.
Unicorn). In Go, it's far more common to use goroutines and multithreaded
programs to achieve the same thing. Memory leaks are less likely (although
still very possible!) and processes can be much more long-lived.

### Goroutines

Ruby has threads and fibers. In 1.8, Ruby's threads were green; in 1.9+, they
map 1:1 with OS threads.

Goroutines are much more similar to Ruby 1.8 threads than they are to anything
else. This means that they're super-lightweight - you can have lots of them,
and you don't need to think hard before firing one up.

Behind the scenes, the Go runtime compiled into your binary manages a set
number of OS threads (`runtime.GOMAXPROCS`) and will schedule goroutines to
run on those threads for you (unlike fibers, which need to be manually
scheduled).

```ruby
Thread.new { ... }
```

```go
go doIt()
go func() { ... }()
go func(x int) { ... }(1)
```

Here's a gotcha with goroutines and `for` loops ([playground](https://play.golang.org/p/GANFcmmTnu)):

```go
package main

import (
	"fmt"
	"sync"
)

func main() {
	var wg sync.WaitGroup
	wg.Add(10)
	for _, x := range []int{0,1,2,3,4,5,6,7,8,9} {
	   go func() {
            fmt.Println(x)
			  wg.Done()
		}()
	}
	wg.Wait()
}
```

This happens because the for loop re-uses the same memory for the variable! `&x`
is the same value each time fmt.Println() is reached. The Go runtime starts ten
new goroutines, but none of them happen to run until the `for` loop is finished,
at which point `x == 4`. This is. So common. I literally cannot even.

The common solution is just to make a copy:

```go
for _, x := range []int{0,1,2,3,4,5,6,7,8,9} {
    x := x
    go func() { ... }()
}
```

Or even:

```go
for _, x := range []int{0,1,2,3,4,5,6,7,8,9} {
    go func(copied int) { ... }(x)
}
```

The former is seen as idiomatic, and makes the copy more obvious than the
latter, but they're both annoying. You will do this at some point.

### Synchronization

Setting a subprocess, thread or goroutine off without guaranteeing that it will
terminate is bad. You generally want to wait for it to finish:

```ruby
thr = Thread.new { ... }
thr.join
```

```go
import "sync"

var wg sync.WaitGroup

wg.Add(1)
go func() { ... ; wg.Done() }()
wg.Wait()
```

Even worse is when you have **shared mutable state**. If you have to have it,
unsynchronized access will introduce many bugs, in Ruby and Go.

Major Ruby data structures like `Hash` and `Array` are not thread-safe, but
in practice, it's rarely an issue due to how people write Ruby code. In Go,
it really matters. Concurrently writing to a slice or map *requires*
synchronization. Luckily, we have mutexes:

```ruby
class X
  def initialize
    @m = Mutex.new
    @x = Hash.new
  end

  def add(k, v)
    @m.synchronize { @x[k] = v }
  end

  def read(k)
    @m.synchronize { @x[k] }
  end
end
```

```go
type X struct {
    sync.Mutex

    x map[string]string
}

func (x *X) Add(k, v string) {
    x.Lock()
    defer x.Unlock()

    x.x[k] = v
}

func (x *X) Read(k string) string {
    x.Lock()
    defer x.Unlock()

    return x.x[k]
}
```

More complicated synchronization primitives can be built up from `sync.Cond`,
which is a [condition value](http://pages.cs.wisc.edu/~remzi/OSTEP/threads-cv.pdf).
If you see it in the wild and it's *not* a reimplementation of Ruby's `Monitor`,
I'm afraid you're on your own.

To make something happen at-most-once, we have `sync.Once`:

```go
package foo

import "sync"

var (
    registry map[string]string
    once sync.Once
)

func Read(k string) string {
    once.Do(func() { registry = make(map[int]struct{}) }
    return registry[k]
}
```

And for this infamous example:

```ruby
i = 0
Thread.new { 1000000.times { i += 1 } }
Thread.new { 1000000.times { i += 1 } }
```

We have `sync/atomic`:

```go
import "sync/atomic"

var i int64

func inc(target *int64) {
    for i := 0; i < 1000000; i++ {
        atomic.AddInt64(target, 1)
    }
}

go inc(&i)
go inc(&i)
```

(Also `atomic.SwapInt64`)

### Channels

Channels are great. When people start using Go, everyone wants to use channels.
For everything. Always make sure that a traditional synchronization primitive
isn't more appropriate. For instance, let's have a concurrent map using channels
instead of mutexes ([playground](https://play.golang.org/p/AogRnKR5ZN))

```go
package main

import (
	"fmt"
)

type tuple struct {
	k string
	v string
}

type req struct {
	tuple
	done chan tuple
}

type DoNotEven struct {
	updates chan req
	reads   chan req
	x       map[string]string
}

func (d *DoNotEven) Add(k, v string) {
	done := make(chan tuple)
	d.updates <- req{tuple{k: k, v: v}, done}
	<-done
}

func (d *DoNotEven) Read(k string) string {
	done := make(chan tuple)
	d.reads <- req{tuple{k: k}, done}
	tuple := <-done

	return tuple.v
}

func (d *DoNotEven) process() {
	for {
		select {
		case r := <-d.updates:
			d.x[r.tuple.k] = r.tuple.v
			close(r.done)
		case r := <-d.reads:
			r.done <- tuple{v: d.x[r.tuple.k]}
		}
	}
}

func main() {
	d := &DoNotEven{
		updates: make(chan req, 100),
		reads:   make(chan req, 100),
		x:       make(map[string]string),
	}
	go d.process()

	d.Add("foo", "bar")
	fmt.Println(d.Read("foo"))
}
```

Yes, this works. Yes, people do this kind of thing on occasion. Just
use a mutex. Seriously.

Anyway, there are two kinds of channels, as shown in this example:
buffered (`make(chan req, 100)`) and unbuffered (`make(chan req)`).

The former is a bit similar to a Ruby queue:

```ruby
q = Queue.new

0.upto(9) {|n| Thread.new { q << n } }

while v = queue.pop
  puts v
end
```

```go
q := make(chan int, 10)

for _, x := range []int{0,1,2,3,4,5,6,7,8,9} {
    go func() { q <- x }(x)
}

for _, v := range q {
    fmt.Println(v)
}
```

However, you need to know a sensible buffer size at the start. Go
won't let items pile up forever - if the channel fills up, the
sender will block.

Buffering is bad, of course. See [bufferbloat](https://en.wikipedia.org/wiki/Bufferbloat).
And don't forget that everything on the buffer will be lost if the process
dies.

No, the purpose of channels is not to be a queue. Buffered channels can
be abused for that, but they're better for *pools*. The size can be used
for concurrency.

Unbuffered channels are another type of **synchronization primitive**. See
how they're used in the `DoNotEven` example. We send a fire-and-forget
request into our "queue" (naughty), but then correctly use an unbuffered
channel to **wait** until the request has been completed.

This is what channels are actually for. Communication between goroutines.
For instance, perhaps you fire off a goroutine to perform a long-running
calculation:

```ruby
thr = Thread.new { do_it }

# do other things

thr.value # blocks until the thread exit
```

```go
ch := make(chan string)
go doIt(ch)

// Do other things

<- ch // Blocks until the goroutine writes to the channel
```

Channels can be used to communicate throughout the goroutine's lifetime. Here's
an example ([playground](https://play.golang.org/p/696o9zuFjF)):

```go
package main

import (
		"fmt"
		"sync"
)

func doIt(start, step int, do <-chan struct{}, done chan<- struct{}) {
	value := start
	for range do {
		fmt.Println(value)
		value += step

		if value > 100 {
			close(done)
			break
		}

		done <- struct{}{}
	}
}

func main() {
	var wg sync.WaitGroup
	odd := make(chan struct{})
	even := make(chan struct{})
	wg.Add(2)

	go func() {
		doIt(0, 2, even, odd)
		wg.Done()
	}()

	go func() {
		doIt(1, 2, odd, even)
		wg.Done()
	}()

	even <- struct{}{}

	wg.Wait()
}
```

How would we do this in Ruby?

```ruby
q1 = Queue.new
q2 = Queue.new
odd  = Thread.new { (0 .. 100).step(2) {|i| q1.pop ; puts i ; q2.push :ok } }
even = Thread.new { (1 ..  99).step(2) {|i| q2.pop ; puts i ; q1.push :ok } }

q1.push :ok
```

Almost. The Go version has a benefit over the Ruby version: the notifying
goroutine *knows* that the notified goroutine has received the message, so
it is no longer the notifier's problem.

With unbuffered channels, Go leans heavily on
[CSP](https://en.wikipedia.org/wiki/Communicating_sequential_processes).


## Tooling and environment

### `$GOROOT` and `$GOPATH`

These environment variables tell the Go runtime where to look for itself and
your code, respectively. Always work inside a `$GOPATH`, and respect where
Go expects your code to be. The pain otherwise just isn't worth it. If you
want a neat directory tree elsewhere, symlink into your `$GOPATH` (or vice
versa).

### GVM ([Go Version Manager](https://github.com/moovweb/gvm))

Does the same job as `rvm`. Ensure you have a system-installed copy of `go` or
it won't be able to compile new go versions!

```
$ gvm use system
Now using version system
$ gvm list

gvm gos (installed)

   go1.5
   go1.6.2
   go1.7
=> system

$ gvm install go1.8rc1
Updating Go source...
Installing go1.8rc1...
 * Compiling...
go1.8rc1 successfully installed!
$ gvm use go1.8rc1
Now using version go1.8rc1
$ go version
go version go1.8rc1 darwin/amd64
$ gvm pkgset create gitaly
$ gvm pkgset use gitaly
Now using version go1.8rc1@gitaly
$ echo $GOROOT
/Users/lupine/.gvm/gos/go1.8rc1
$ echo $GOPATH
/Users/lupine/.gvm/pkgsets/go1.8rc1/gitaly:/Users/lupine/.gvm/pkgsets/go1.8rc1/global
```

Use a separate pkgset (and so `$GOPATH`) per project you need to work on, and
use the earliest version of go that the project supports (right now: usually
go1.5, sometimes go1.7).

### Dependency management

Go expects all imported packages to be inside `$GOPATH` or `$GOROOT` at build
time. You can use `go get` to pull the latest version of a package into your
`$GOPATH`, but this has no versioning, and relying on it is likely to result
in dependency hell.

Ruby has `bundler`, with `Gemfile` and `Gemfile.lock`, to specify packages and
dependencies. In Go, it's more typical to vendor your dependencies directly
into your source tree, using the `GO15VENDOREXPERIMENT` method. This may seem
odd, but it's considered good practice for projects that produce a binary.

From go1.6 (and go1.5 if `GO15VENDOREXPERIMENT=1`), Go will look for a `vendor/`
directory in every package it compiles. If you `import "foo"` in your package,
and `vendor/foo` exists, that package will be used before any packages in
`$GOPATH/src/foo`.

If your project is a library, vendoring is strongly discouraged. The Go
community has high standards around backwards compatibility and keeping
dependency trees shallow, so the lack of a good solution here isn't as
terrifying as it sounds. Most of the time.

A common idiom is to import a library package via a package registry that
embeds a version identifier in the package name. This is usually
[`gopkg.in`](https://gopkg.in), so you might see `import` statements like:

```go
package foo

import (
	"gopkg.in/check.v1"
	"gopkg.in/yaml.v2"
)
```

These versioned URLs can be used for vendored code as well, of course.

Managing `vendor/` trees full of other peoples' code is tricky, so several tools
exist to make this easier. [`Godep`](https://github.com/tools/godep) and
[`govendor`](https://github.com/kardianos/govendor) are commonly used.

#### Godep

* Used by `gitlab-workhorse`, `gitlab-ci-multi-runner`, `gitlab-pages`
* Precedes `GO15VENDOREXPERIMENT`.
* A `Gemfile.lock` equivalent is stored in `Godeps/Godeps.json`
* Vendored files are placed into `vendor/` or `Godeps/_workspace`
* Dependencies are staged through `$GOPATH`

Usage looks like:

```
$ go get gitub.com/tools/godep
$ godep restore
# Downloads all packages in `Godeps/Godeps.json` and checks them out into
# `$GOPATH`.
$ godep save ./...
# Copies files in referenced packages from `$GOPATH` to vendor directory
# and updates `Godeps/Godeps.json` with packages + versions
$ godep update <package-name>
#
```

The expected workflow is that you `godep restore`, make changes to your
dependencies in the `$GOPATH`, then `godep save` or `godep update` to persist
those changes to your vendored copy.

#### Govendor

* Used by `gitaly`
* Written *after* `GO15VENDOREXPERIMENT`
* A `Gemfile.lock` equivalent is stored in `vendor/vendor.json`
* Dependencies do not need to be staged through `$GOPATH`

Usage looks like:

```
$ go get github.com/kardianos/Govendor
$ govendor fetch +missing
# Downloads any referenced packages directly into `vendor/`
$ govendor remove +unused
# Deletes any packages in vendor/ that are not referenced
```

### Documentation

`godoc` is a built-in, very simple inline code documentation tool

* Standard library: [`https://golang.org/pkg`](https://golang.org/pkg)
* Everything else: [`https://godoc.org`](https://godoc.org)
* Run `godoc` locally: `godoc -http=127.0.0.1:8080`

### Linting and checking

* `go fmt`
* `go test -bench`
* `go test -benchmem`
* `go test -cover`
* `go test -cpu 1,2,4`
* `go test -cpuprofile`
* `go test -memprofile`
* `go test -race` / `go build -race`
* `go test -trace`
* [gometalinter](https://github.com/alecthomas/gometalinter)

A comprehensive test suite can help you find all sorts of problems in
your actual code. Standards are high in the Go community - `go fmt` is
mandatory, and these linters and checkers are often seen as essential
too. Especially the race detector.

Go's benchmark support is also top-class. Consider writing a benchmark
when investigating performance issues.

## Resources

### Awesome reading

* [The tour!](https://dave.cheney.net/resources-for-new-go-programmers)
* [Dave Cheney's resources for new Go programmers](https://dave.cheney.net/resources-for-new-go-programmers)
* [Effective Go](https://golang.org/doc/effective_go.html)
* [Functional options for friendly APIs](https://dave.cheney.net/2014/10/17/functional-options-for-friendly-apis)
* [Official Reference Guide](https://golang.org/ref)
    * [Language Specification](https://golang.org/ref/spec)
* [Official blog](https://blog.golang.org/index)
    * [JSON and Go](https://blog.golang.org/json-and-go)
    * [Go slices: usage and internals](https://blog.golang.org/go-slices-usage-and-internals)
    * [Arrays, slices and 'append'](https://blog.golang.org/slices)
    * [Strings, bytes, runes and characters](https://blog.golang.org/strings)
    * [Share memory by communicating](https://blog.golang.org/share-memory-by-communicating)
* [Golang Weekly](http://golangweekly.com/)


### Awesome libraries

* Database
    * https://github.com/jmoiron/sqlx
    * https://github.com/Go-SQL-Driver/MySQL
    * https://github.com/lib/pq
    * https://github.com/garyburd/redigo
    * https://github.com/prometheus/client_golang
* HTTP
    * https://github.com/gorilla/handlers
    * https://github.com/gorilla/mux
    * https://github.com/gorilla/websocket
* Miscellaneous
    * https://github.com/Sirupsen/logrus
    * https://github.com/urfave/cli
* Serialization
    * https://github.com/BurntSushi/toml
    * https://github.com/golang/protobuf
    * https://gopkg.in/yaml.v2

### Editor war \o/

TODO
